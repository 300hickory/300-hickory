A quiet setting tucked away from the hustle and bustle of everyday life. Hickory Trace Apartments offers peaceful, relaxing surroundings only minutes from the Global Mall, Ford Ice Center, Public Library, Nashville State College and downtown Nashville and Percy Priest Lake.

Address: 300 Hickory Hollow Place, Antioch, TN 37013, USA

Phone: 615-247-5373
